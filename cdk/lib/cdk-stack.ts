import * as cdk from "aws-cdk-lib";
import * as lambda from "aws-cdk-lib/aws-lambda";
import * as events from 'aws-cdk-lib/aws-events'
import * as kinesis from "aws-cdk-lib/aws-kinesis";
import * as targets from 'aws-cdk-lib/aws-events-targets';
import * as iam from "aws-cdk-lib/aws-iam";
import * as path from "path";
import { Construct } from "constructs";
import { Stage } from "./constant";
// import * as sqs from 'aws-cdk-lib/aws-sqs';

interface CdkStackProps extends cdk.StackProps{
  stage: Stage
}

export class CdkStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props:CdkStackProps) {
    super(scope, id, props);
    const {stage} = props;
    const prefix = stage;

    const myEventBus =  events.EventBus.fromEventBusArn(this,`${prefix}-DefaultBus`,'arn:aws:events:ap-south-1:323439077171:event-bus/default');

    const myEventRule = new events.Rule(this, `${prefix}-MyEventRule`, {
      eventBus: myEventBus, // Associate the rule with the event bus
      eventPattern: {
        source: ['*']
      }
    });

    const stream = new kinesis.Stream(this, `${prefix}-MyKinesisStream`, {
      streamName: `${prefix}-MyDataStream`,
      streamMode: kinesis.StreamMode.ON_DEMAND,
    });

    myEventRule.addTarget(new targets.KinesisStream(stream));

    const lambdaRole = new iam.Role(this, `${prefix}-LambdaRole`, {
      assumedBy: new iam.ServicePrincipal("lambda.amazonaws.com"),
    });

    lambdaRole.addToPolicy(
      new iam.PolicyStatement({
        actions: [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents",
        ],
        resources: ["*"],
      })
    );

    stream.grantRead(lambdaRole)
    myEventBus.grantPutEventsTo(lambdaRole)

    const lambdaFunction = new lambda.Function(this, `${prefix}-MyLambdaFunction`, {
      runtime: lambda.Runtime.NODEJS_18_X,
      handler: "index.handler",
      code: lambda.Code.fromAsset(path.join("../lambdas/dist")),
      role: lambdaRole,
    });

    const lambdaFunction1 = new lambda.Function(this, `${prefix}-MyLambdaFunctionTest`, {
      runtime: lambda.Runtime.NODEJS_18_X,
      handler: "index.handler",
      code: lambda.Code.fromAsset(path.join("../lambdas/dist")),
      role: lambdaRole,
    });

    myEventRule.addTarget(new targets.LambdaFunction(lambdaFunction1));


    lambdaFunction.addEventSourceMapping(`${prefix}-KinesisEventSource`,{
      eventSourceArn:stream.streamArn,
      batchSize: 10,
      startingPosition: lambda.StartingPosition.LATEST,
      maxBatchingWindow: cdk.Duration.seconds(2)
    })
  }
}
