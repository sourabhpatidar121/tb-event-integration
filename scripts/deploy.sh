export STAGE=$1
echo $STAGE
cd cdk
npm install -g aws-cdk
apt-get update
apt-get install -y awscli
aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
aws configure set region $AWS_DEFAULT_REGION
cdk deploy --all --require-approval never