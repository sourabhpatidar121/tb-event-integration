import {
  EventBridgeClient,
  PutEventsCommand,
  PutEventsRequestEntry,
} from "@aws-sdk/client-eventbridge";

import {
  Context,
  Handler,
  KinesisStreamEvent,
  KinesisStreamRecord,
} from "aws-lambda";

const eventBridgeClient = new EventBridgeClient({ region: "ap-south-1" });

const eventBusName = "default";
const source = "your.application";
const detailType = "custom.event";

// Create the EventBridge event

const processRecord = (record: KinesisStreamRecord): PutEventsRequestEntry => {
  const detail = record.kinesis.data;
  return {
    EventBusName: eventBusName,
    Source: source,
    DetailType: detailType,
    Detail: detail,
  };
};

export const handler: Handler = async (
  event: KinesisStreamEvent,
  context: Context
) => {
  console.log(JSON.stringify(event));
  const entries = event.Records.map(processRecord);
  const putEventCommand = new PutEventsCommand({
    Entries: entries,
  });
  await eventBridgeClient.send(putEventCommand);
};
